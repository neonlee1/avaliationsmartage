import React from 'react';
import ResultPage from './pages/resultPage/index.js';
import './assets/styles/global.css';
function App() {

  return (
    <ResultPage />
  );
}

export default App;