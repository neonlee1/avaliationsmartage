import styled from 'styled-components';

const Button = styled.button `
font-size: 1em;
margin: 1em;
padding: 0.5rem;
border: 2px;
background-color: grey;
color:white;
border-radius: 3px;
`;
const Input = styled.input `
font-size: 18px;
padding: 3px;
margin: 10px;
display:flex;
border: 1px solid;
border-radius: 3px;
::placeholder {
  color: black;
  border:none;
}
`;

const Form = styled.form `
display: flex;
align-items: center;
justify-content: center`;

const CenterPageDiv = styled.div `
display: flex;
align-items: center;
justify-content: center`;

const Text = styled.div `
display: grid;
font-size: 50px;
display: flex;
align-items: center;
justify-content: center
`;

const Title = styled.h1 `
display: flex;
align-items: center;
justify-content: center;
color: white;
margin: 0`;


const Card = styled.div `
border: 2px solid green;
border-radius: 10px;
display:flex;
justify-content: space-around;
padding: 10px;
margin: 10px;`;

const Header = styled.div `
background-color: green;
margin: 0px;
`;

const Subtitle = styled.h1 `
color:green; 
font-size 1.2rem;`;

const Option = styled.span `
font-weight: 700`;

const Container = styled.div `
padding: 1rem`;

export default Button;
export { Input, CenterPageDiv, Text, Card, Form, Header, Title, Subtitle, Option, Container }