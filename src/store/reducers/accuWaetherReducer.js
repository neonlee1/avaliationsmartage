export default function(state, action) {
    switch (action.type) {
        case "API_ACCUWEATHER":
            return {
                ...state,
                dataWeather: action.payload
            }
        case "API_ACCUWEATHER_ERROR":
            return {...state, dataWeather: action.payload }
        default:
            return { dataWeather: "" }
    }

}