export default function(state, action) {
    switch (action.type) {
        case "API_KIWI":
            return {
                ...state,
                dataKiwiApi: action.payload
            }
        case "API_KIWI_ERROR":
            return {...state,
                dataKiwiApi: action.payload
            }
        default:
            return { dataKiwiApi: "" }
    }

}