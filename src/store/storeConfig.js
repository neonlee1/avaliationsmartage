import { combineReducers } from 'redux';
import kiwiApiReducer from './reducers/kiwiApiReducer';
import accuWeatherReducer from './reducers/accuWaetherReducer';

const reducer = combineReducers({
    kiwi: kiwiApiReducer,
    accuWeather: accuWeatherReducer
})

export default reducer;