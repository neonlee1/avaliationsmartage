import kiwiApi from '../../services/kiwiApi';


export const kiwiApiAction = (flyFrom, flyTo, going, returnDate) => {
    return [dispatch => {
        try {
            kiwiApi.get('/search?fly_from=' + flyFrom + '&fly_to=' + flyTo + '&date_from=' + going + '&date_to=' + returnDate)
                .then(response => {
                    dispatch({ type: 'API_KIWI', payload: response.data });
                });
        } catch (error) {
            dispatch({ type: 'API_KIWI_ERROR', payload: { error } });

        }

    }]
}