import weatherApi from '../../services/weatherApi';
import WeatherApi from '../../services/weatherApi';


export const AccuWeatherAction = (flyFrom) => {
    return [dispatch => {
        try {
            WeatherApi.post('/locations/v1/cities/search?apikey=9NGsjSEdzBn1M7Lxgwr6xmlTsP7Dg8xA&q=' + flyFrom)
                .then(response => {
                    weatherApi.get('/forecasts/v1/daily/5day/2121532?apikey=9NGsjSEdzBn1M7Lxgwr6xmlTsP7Dg8xA').then(responseweatherApi => {
                        dispatch({ type: 'API_ACCUWEATHER', payload: responseweatherApi.data });
                    });

                });
        } catch (error) {
            dispatch({ type: 'API_ACCUWEATHER_ERROR', payload: { error } });
        }

    }]

}