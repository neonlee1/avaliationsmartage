import axios from 'axios';

const weatherApi = axios.create({
    baseURL: 'http://dataservice.accuweather.com'
});

export default weatherApi;