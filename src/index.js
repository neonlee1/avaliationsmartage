import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import storeConfig from './store/storeConfig.js';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import multi from 'redux-multi';

const store = applyMiddleware(thunk, multi, promise)(createStore)(storeConfig)
ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode >
      < App />
    </React.StrictMode>
  </Provider>,

  document.getElementById('root')
);