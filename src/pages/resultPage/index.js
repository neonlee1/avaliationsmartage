import React from 'react';
import Button, { Input, Form, Card, Header, Title, Subtitle, Option, CenterPageDiv, Container } from '../../components/index.js';
import { kiwiApiAction } from '../../store/actions/kiwiActions';
import { AccuWeatherAction } from '../../store/actions/accuWaetherActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

function ResultPage(props) {
    const [originCity, setOriginCity] = React.useState("");
    const [destinyCity, setDestinyCity] = React.useState("");
    const [going, setGoing] = React.useState("");
    const [returnDate, setReturnDate] = React.useState("");
    const { kiwi, accuWeather } = props;

    function submitForm(e) {
        e.preventDefault();
        props.AccuWeatherAction(originCity);

        props.kiwiApiAction(originCity, destinyCity, going, returnDate);

    }
    return (
        <div>
            <Header>
                <div>
                    <Title>
                        Best Place to go
                    </Title>
                </div>
                <Form onSubmit={submitForm}>
                    <Input type="text" placeholder="Origin" onChange={(e) => { setOriginCity(e.target.value) }} />
                    <Input type="text" placeholder="Destiny" onChange={(e) => { setDestinyCity(e.target.value) }} />
                    <Input type="date" placeholder="Going" onChange={(e) => { setGoing(e.target.value) }} />
                    <Input type="date" placeholder="Return" onChange={(e) => { setReturnDate(e.target.value) }} />
                    <Button type="submit">Search</Button>
                </Form>
            </Header>
            {accuWeather ? <h1>temperature</h1> : null}
            <CenterPageDiv>
                {accuWeather ? accuWeather['DailyForecasts'].map((temperature, index) => {
                    return <Card key={index}>
                        <Subtitle>{temperature['Date']}</Subtitle>
                        <br />
                        <div>
                            <span>Maximum {temperature['Temperature']['Maximum']['Value']} - </span>
                            <br />

                            <span>Minimum {temperature['Temperature']['Minimum']['Value']}</span>
                            <br />
                        </div>
                        <div>
                            <span>Day {temperature['Day']['IconPhrase']}</span>
                            <span>Night {temperature['Night']['IconPhrase']}</span>
                        </div>
                    </Card>
                }) : null}

            </CenterPageDiv>
            {kiwi ? <h1>Travels</h1> : null}
            {kiwi ? kiwi['data'].map((kiwi) => {
                return <Card key={kiwi.id}>
                    <div>
                        <Subtitle>Airlines</Subtitle>
                        {kiwi['airlines'].map((airlines, index) => {
                            return <Option key={index}>{airlines}  </Option>
                        })}
                        <p>{kiwi['cityFrom']} - {kiwi['cityTo']}</p>
                    </div>
                    <div>
                        <Subtitle>Informations</Subtitle>
                        <Option>Price travel: </Option>
                        <span>${kiwi['conversion']['EUR']},00</span>
                        <br />
                        <Option>Country from: </Option>
                        <span>{kiwi['countryFrom']['name']}</span>
                        <br />
                        <Option>Country To: </Option>
                        <span>{kiwi['countryTo']['name']}</span>
                        <br />
                        <Option>Quality: </Option>
                        <span>{kiwi['quality']}</span>
                        <br />
                        <Option>Distance: </Option>
                        <span>{kiwi['distance']}</span>
                        <br />

                    </div>
                    <div>
                        <div> <Subtitle>Time Travel</Subtitle>
                            <span>{Date(kiwi['local_arrival'])} </span>
                            <br />
                        </div>
                        <Subtitle>Routes</Subtitle>
                        {kiwi['route'].map((route, index) => {
                            return <Option key={index}>{route['cityCodeFrom']}  </Option>
                        })}
                    </div>
                    <div>
                        <Subtitle>Bags Price</Subtitle>
                        ${kiwi['bags_price']['1']}
                        <br />
                        {kiwi['bags_price']['2'] === undefined ? "" : "$" + kiwi['bags_price']['2']}
                    </div>
                </Card>
            }) : null}
        </div >
    );
}
const mapStateToProps = state => ({ kiwi: state.kiwi.dataKiwiApi, accuWeather: state.accuWeather.dataWeather })

const mapDispatchToProps = dispatch => bindActionCreators({ kiwiApiAction, AccuWeatherAction }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ResultPage);